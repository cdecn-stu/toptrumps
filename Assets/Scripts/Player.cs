﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class Player
{
    public Queue<Card> cards;
    public int playerID { get; }
    public string playerName;
    public GameObject cardModel { get; }

    public Card currentCard;
    public bool currentPicker;
    public int numberOfCardsLeft;

    public Player(int IDofPlayer)
    {
        cards = new Queue<Card>();
        playerID = IDofPlayer;
        playerName = "Player " + (playerID + 1);
        cardModel = UnityEngine.MonoBehaviour.Instantiate(Resources.Load<GameObject>("CardModel"));
        cardModel.transform.position = new Vector3((cardModel.transform.localScale.x + (cardModel.transform.GetChild(0).transform.localScale.x * 4)) * playerID, 3.5f, 0);
        currentPicker = false;
        numberOfCardsLeft = cards.Count;
    } 

    public void AddCard(Card card)
    {
        cards.Enqueue(card);
        currentCard = cards.Peek();
    }

    public void AddCards(Queue<Card> cardsToAdd)
    {
        foreach (Card card in cardsToAdd)
        {
            cards.Enqueue(card);
        }
    }

    public void DisplayCard()
    {
        Transform ComponentToChange = cardModel.transform.GetChild(0).GetChild(0);
        cardModel.transform.GetChild(0).rotation = new Quaternion(0, 1, 0, 0);

        ComponentToChange.GetChild(0).GetComponent<Image>().sprite = Resources.Load<Sprite>(currentCard.ImageName);
        ComponentToChange.GetChild(1).GetComponent<Text>().text = currentCard.Name;
        if (GameController.deck == "characters")
        {
            ComponentToChange.GetChild(2).GetComponent<Text>().text = "Games: " + currentCard.FirstStat;
            ComponentToChange.GetChild(3).GetComponent<Text>().text = "Spin-Off Series: " + currentCard.SecondStat;
            ComponentToChange.GetChild(4).GetComponent<Text>().text = "First Appearance: " + currentCard.ThirdStat;
            ComponentToChange.GetChild(5).GetComponent<Text>().text = "Latest Appearance: " + currentCard.FourthStat;
            ComponentToChange.GetChild(6).GetComponent<Image>().sprite = Resources.Load<Sprite>("ssbulogo");
        }
        else
        {
            ComponentToChange.GetChild(2).GetComponent<Text>().text = "Games: " + string.Format("{0:n0}", currentCard.FirstStat);
            ComponentToChange.GetChild(3).GetComponent<Text>().text = "Sales: " + string.Format("{0:n0}", currentCard.SecondStat);
            ComponentToChange.GetChild(4).GetComponent<Text>().text = "Release Date: " + currentCard.ThirdStat;
            ComponentToChange.GetChild(5).GetComponent<Text>().text = "Latest Game: " + currentCard.FourthStat;
            ComponentToChange.GetChild(6).GetComponent<Image>().sprite = Resources.Load<Sprite>("mdlogo");
        }
        ComponentToChange.GetChild(7).GetComponent<Text>().text = "Cards Left: " + numberOfCardsLeft.ToString();
        ComponentToChange.GetChild(8).GetComponent<Text>().text = playerName;
    }

    public void HideCard()
    {
        cardModel.transform.GetChild(0).rotation = new Quaternion(1, 0, 0, 0);
    }
    
    public void SetCurrentCard(bool requeueCard)
    {
        Card temp = currentCard;
        cards.Dequeue();

        if (requeueCard == true)
        {
            cards.Enqueue(temp);
        }

        if (cards.Count > 0)
        {
            currentCard = cards.Peek();
        }
    }

    public void UpdateCardsLeft()
    {
        numberOfCardsLeft = cards.Count;
    }
}
