﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public static class SaveSystem
{
    public static void SavePlayer(Player player)
    {
        BinaryFormatter formatter = new BinaryFormatter();
        string filePath = Application.dataPath + "/player" + player.playerID + ".save";
        FileStream stream = new FileStream(filePath, FileMode.Create);

        PlayerData data = new PlayerData(player);

        formatter.Serialize(stream, data);
        stream.Close();
    }

    public static void SaveCardsToAdd(Card[] cards)
    {
        BinaryFormatter formatter = new BinaryFormatter();
        string filePath = Application.dataPath + "/cardstoadd.save";
        FileStream stream = new FileStream(filePath, FileMode.Create);

        formatter.Serialize(stream, cards);
        stream.Close();
    }

    public static PlayerData LoadPlayer(int playerID)
    {
        string filePath = Application.dataPath + "/player" + playerID + ".save";
        if (File.Exists(filePath))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(filePath, FileMode.Open);

            PlayerData data = (PlayerData)formatter.Deserialize(stream);
            stream.Close();

            return data;
        }
        else
        {
            Debug.Log("No file");
            return null;
        }
    }

    public static Card[] LoadCardsToAdd()
    {
        string filePath = Application.dataPath + "/cardstoadd.save";
        if (File.Exists(filePath))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(filePath, FileMode.Open);

            Card[] data = (Card[])formatter.Deserialize(stream);
            stream.Close();

            return data;
        }
        else
        {
            Debug.Log("No file");
            return null;
        }
    }
}
