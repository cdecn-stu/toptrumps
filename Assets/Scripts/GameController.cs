﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : Singleton<GameController>
{
    public enum Categories
    {
        FIRST,
        SECOND,
        THIRD,
        FOURTH
    }

    public static int numberOfPlayers = 2;
    public static int numberOfCardsToUse;
    public static string deck;
    public static bool pauseGameInput = false;
    public static bool aiGame = false;
    public static Player winningPlayer;
    public static Queue<Player> players;
    public static Queue<Card> cardsToAdd = new Queue<Card>();

    public AudioClip cardTurn;

    private static UnityEngine.UI.Text continueText;
    private static UnityEngine.UI.Text resultText;
    private static UnityEngine.UI.Text cardsToAddText;

    private Card[] cards;
    private Queue<Card> sortedUsedCards = new Queue<Card>();

    private void AddCardsToWinner(Player player)
    {
        player.AddCards(cardsToAdd);
    }

    private void ChooseWinningCard(Card winningCard)
    {
        winningPlayer = FindOwnerOfWinningCard(winningCard);

        foreach (Player player in players)
        {
            player.currentPicker = false;
        }

        winningPlayer.currentPicker = true;

        AddCardsToWinner(winningPlayer);

        do
        {
            cardsToAdd.Dequeue();
        } while (cardsToAdd.Count > 0);
        cardsToAddText.enabled = false;

        ShowAllCurrentCards();
        pauseGameInput = true;
        StartCoroutine("WaitForInput", false);
    }

    private void CompareFirstCategory(Card[] cardsToTest)
    {
        foreach (Player player in players)
        {
            player.cardModel.transform.GetChild(0).GetChild(0).GetChild(2).GetComponent<UnityEngine.UI.Text>().color = Color.cyan;
            player.cardModel.transform.GetChild(0).GetChild(0).GetChild(2).GetComponent<UnityEngine.UI.Outline>().effectColor = Color.black;
        }

        Card winningCard = new Card("", "", 0, 0, "31/12/9999", "01/01/0001 ");
        bool draw = false;

        foreach (Card card in cardsToTest)
        {
            if (card.FirstStat > winningCard.FirstStat)
            {
                winningCard = card;
                draw = false;
            }
            else if (card.FirstStat == winningCard.FirstStat)
            {
                draw = true;
            }
        }

        if (draw == false)
        {
            foreach (Card card in cardsToTest)
            {
                if (card != winningCard)
                {
                    cardsToAdd.Enqueue(card);
                }
            }

            ChooseWinningCard(winningCard);
        }
        else
        {
            foreach (Card card in cardsToTest)
            {
                cardsToAdd.Enqueue(card);
            }

            ShowAllCurrentCards();
            pauseGameInput = true;
            StartCoroutine("WaitForInput", true);

        }
    }

    private void CompareSecondCategory(Card[] cardsToTest)
    {
        foreach (Player player in players)
        {
            player.cardModel.transform.GetChild(0).GetChild(0).GetChild(3).GetComponent<UnityEngine.UI.Text>().color = Color.cyan;
            player.cardModel.transform.GetChild(0).GetChild(0).GetChild(3).GetComponent<UnityEngine.UI.Outline>().effectColor = Color.black;
        }

        Card winningCard = new Card("", "", 0, 0, "31/12/9999", "01/01/0001");
        bool draw = false;

        foreach (Card card in cardsToTest)
        {
            if (card.SecondStat > winningCard.SecondStat)
            {
                winningCard = card;
                draw = false;
            }
            else if (card.SecondStat == winningCard.SecondStat)
            {
                draw = true;
            }
        }

        if (draw == false)
        {
            foreach (Card card in cardsToTest)
            {
                if (card != winningCard)
                {
                    cardsToAdd.Enqueue(card);
                }
            }

            ChooseWinningCard(winningCard);
        }
        else
        {
            foreach (Card card in cardsToTest)
            {
                cardsToAdd.Enqueue(card);
            }

            ShowAllCurrentCards();
            pauseGameInput = true;
            StartCoroutine("WaitForInput", true);
        }
    }

    private void CompareThirdCategory(Card[] cardsToTest)
    {
        foreach (Player player in players)
        {
            player.cardModel.transform.GetChild(0).GetChild(0).GetChild(4).GetComponent<UnityEngine.UI.Text>().color = Color.cyan;
            player.cardModel.transform.GetChild(0).GetChild(0).GetChild(4).GetComponent<UnityEngine.UI.Outline>().effectColor = Color.black;
        }

        Card winningCard = new Card("", "", 0, 0, "31/12/9999", "01/01/0001");
        bool draw = false;

        foreach (Card card in cardsToTest)
        {
            int year = int.Parse(card.ThirdStat.Substring(6, 4));
            int month = int.Parse(card.ThirdStat.Substring(3, 2));
            int day = int.Parse(card.ThirdStat.Substring(0, 2));
            DateTime date = new DateTime(year, month, day);

            int wYear = int.Parse(winningCard.ThirdStat.Substring(6, 4));
            int wMonth = int.Parse(winningCard.ThirdStat.Substring(3, 2));
            int wDay = int.Parse(winningCard.ThirdStat.Substring(0, 2));
            DateTime wDate = new DateTime(wYear, wMonth, wDay);

            if (date < wDate)
            {
                winningCard = card;
                draw = false;
            }
            else if (date == wDate)
            {
                draw = true;
            }
        }

        if (draw == false)
        {
            foreach (Card card in cardsToTest)
            {
                if (card != winningCard)
                {
                    cardsToAdd.Enqueue(card);
                }
            }

            ChooseWinningCard(winningCard);
        }
        else
        {
            foreach (Card card in cardsToTest)
            {
                cardsToAdd.Enqueue(card);
            }

            ShowAllCurrentCards();
            pauseGameInput = true;
            StartCoroutine("WaitForInput", true);
        }
    }

    private void CompareFourthCategory(Card[] cardsToTest)
    {
        foreach (Player player in players)
        {
            player.cardModel.transform.GetChild(0).GetChild(0).GetChild(5).GetComponent<UnityEngine.UI.Text>().color = Color.cyan;
            player.cardModel.transform.GetChild(0).GetChild(0).GetChild(5).GetComponent<UnityEngine.UI.Outline>().effectColor = Color.black;
        }

        Card winningCard = new Card("", "", 0, 0, "31/12/9999", "01/01/0001");
        bool draw = false;

        foreach (Card card in cardsToTest)
        {
            int year = int.Parse(card.FourthStat.Substring(6, 4));
            int month = int.Parse(card.FourthStat.Substring(3, 2));
            int day = int.Parse(card.FourthStat.Substring(0, 2));
            DateTime date = new DateTime(year, month, day);

            int wYear = int.Parse(winningCard.FourthStat.Substring(6, 4));
            int wMonth = int.Parse(winningCard.FourthStat.Substring(3, 2));
            int wDay = int.Parse(winningCard.FourthStat.Substring(0, 2));
            DateTime wDate = new DateTime(wYear, wMonth, wDay);

            if (date > wDate)
            {
                winningCard = card;
                draw = false;
            }
            else if (date == wDate)
            {
                draw = true;
            }
        }

        if (draw == false)
        {
            foreach (Card card in cardsToTest)
            {
                if (card != winningCard)
                {
                    cardsToAdd.Enqueue(card);
                }
            }

            ChooseWinningCard(winningCard);
        }
        else
        {
            foreach (Card card in cardsToTest)
            {
                cardsToAdd.Enqueue(card);
            }

            ShowAllCurrentCards();
            pauseGameInput = true;
            StartCoroutine("WaitForInput", true);
        }
    }

    private void CreatePlayers()
    {
        players = new Queue<Player>();
        for (int i = 0; i < numberOfPlayers; i++)
        {
            if (i == 0 || aiGame == false)
            {
                Player player = new Player(i);
                if (i == 0)
                {
                    player.currentPicker = true;
                }
                players.Enqueue(player);
                if (MenuScript.playerNames[i] != null)
                {
                    player.playerName = MenuScript.playerNames[i];
                }

                player.cardModel.GetComponent<Camera>().rect = new Rect(1 / 1 / (float)numberOfPlayers * i, 0f, 1 / 1 / (float)numberOfPlayers, 1f);
            }
            else
            {
                Player ai = new Player(i);
                ai.playerName = "CPU";
                ai.currentPicker = false;
                players.Enqueue(ai);

                ai.cardModel.GetComponent<Camera>().rect = new Rect(1 / 1 / (float)numberOfPlayers * i, 0f, 1 / 1 / (float)numberOfPlayers, 1f);
            }
        }
    }

    private void DealCards()
    {
        Dealer dealer = new Dealer(cards);

        if (cards.Length == numberOfCardsToUse)
        {
            while (dealer.HasCardsLeft())
            {
                Player player = players.Dequeue();
                player.AddCard(dealer.GetNextCard());
                players.Enqueue(player);
            }
        }
        else
        {
            for (int i = 0; i < numberOfCardsToUse; i++)
            {
                Player player = players.Dequeue();
                player.AddCard(dealer.GetNextCard());
                players.Enqueue(player);
            }
        }
    }

    public void EvaluateCardFromPlayer(Categories category)
    {
        Card[] cardsToTest = GetAllCurrentCards();
        bool aiTurn = false;

        if (aiGame == true)
        {
            foreach (Player player in players)
            {
                if (player.currentPicker == true && player.playerName.Contains("CPU"))
                {
                    aiTurn = true;
                }
            }
        }

        if (aiTurn == false)
        {
            switch (category)
            {
                case Categories.FIRST:
                    CompareFirstCategory(cardsToTest);
                    break;
                case Categories.SECOND:
                    CompareSecondCategory(cardsToTest);
                    break;
                case Categories.THIRD:
                    CompareThirdCategory(cardsToTest);
                    break;
                case Categories.FOURTH:
                    CompareFourthCategory(cardsToTest);
                    break;
            }
        }
    }

    private Player FindOwnerOfWinningCard(Card winningCard)
    {
        Player winningPlayer = null;
        int i = 0;

        foreach (Player player in players)
        {
            if (player.currentCard == winningCard)
            {
                winningPlayer = player;
                i++;
            }
        }

        return winningPlayer;
    }

    private Card[] GetAllCurrentCards()
    {
        Card[] currentCards = new Card[numberOfPlayers];
        int i = 0;

        foreach (Player player in players)
        {
            currentCards[i] = player.currentCard;
            i++;
        }
        return currentCards;
    }

    private void HideAllCards()
    {
        foreach (Player player in players)
        {
            player.HideCard();
        }
    }

    public void NewGame()
    {
        continueText = GameObject.Find("ContinueText").GetComponent<UnityEngine.UI.Text>();
        resultText = GameObject.Find("ResultText").GetComponent<UnityEngine.UI.Text>();
        cardsToAddText = GameObject.Find("CardsToAdd").GetComponent<UnityEngine.UI.Text>();
        cardsToAdd = new Queue<Card>();
        sortedUsedCards = new Queue<Card>();

        if (MenuScript.loadGame == false)
        {
            cards = MyUtilities.LoadXMLData(deck);
            CreatePlayers();
            DealCards();
            cardsToAddText.enabled = false;
        }
        else
        {
            MenuScript.loadGame = false;

            Card[] cardsToAddTemp = SaveSystem.LoadCardsToAdd();
            foreach (Card card in cardsToAddTemp)
            {
                cardsToAdd.Enqueue(card);
            }
            if (cardsToAdd.Count > 0)
            {
                cardsToAddText.text = "Cards to add: " + cardsToAdd.Count;
                cardsToAddText.enabled = true;
            }
            else
            {
                cardsToAddText.enabled = false;
            }

            players = new Queue<Player>();
            for (int i = 0; i < numberOfPlayers; i++)
            {
                PlayerData data = SaveSystem.LoadPlayer(i);
                if (data != null)
                {
                    Player player = new Player(data.id);
                    Debug.Log(player.playerID);
                    player.playerName = data.name;
                    Debug.Log(player.playerName);
                    player.currentPicker = data.currentPicker;
                    Debug.Log(player.currentPicker);

                    Queue<Card> cards = new Queue<Card>();
                    Debug.Log(cards.Count);
                    for (int x = 0; x < data.numberOfCards; x++)
                    {
                        Card card = new Card(data.cardsNames[x], data.cardsImageNames[x], data.cardsFirstStat[x], data.cardsSecondStat[x], data.cardsThirdStat[x], data.cardsFourthStat[x]);
                        player.AddCard(card);
                        Debug.Log(card.Name);
                    }
                    player.numberOfCardsLeft = data.numberOfCards;
                    Debug.Log(player.numberOfCardsLeft);

                    players.Enqueue(player);
                    player.cardModel.GetComponent<Camera>().rect = new Rect(1 / 1 / (float)numberOfPlayers * i, 0f, 1 / 1 / (float)numberOfPlayers, 1f);
                    Debug.Log(players.Peek().playerName);
                }
                else
                {
                    i = numberOfPlayers;
                    cards = MyUtilities.LoadXMLData(deck);
                    CreatePlayers();
                    DealCards();
                }
            }
        }
        if (numberOfPlayers <= 3)
        {
            foreach (Player player in players)
            {
                player.cardModel.GetComponent<Camera>().fieldOfView = 35f;
            }
        }
        else if (numberOfPlayers <= 5 && numberOfPlayers > 3)
        {
            foreach (Player player in players)
            {
                player.cardModel.GetComponent<Camera>().fieldOfView = 45f;
            }
        }
        else
        {
            foreach (Player player in players)
            {
                player.cardModel.GetComponent<Camera>().fieldOfView = 55f;
            }
        }

        continueText.enabled = false;
        resultText.enabled = false;

        StartNewTurn();
    }

    private void SetCurrentCardForEachPlayer(Player winningPlayer)
    {
        foreach (Player player in players)
        {
            if (player == winningPlayer)
            {
                player.SetCurrentCard(true);
            }
            else
            {
                player.SetCurrentCard(false);
            }
        }
    }

    public void ShowAllCurrentCards()
    {
        foreach (Player player in players)
        {
            player.DisplayCard();
            Debug.Log("Player " + player.playerID + " has " + player.currentCard.Name);
        }
    }

    private void ShowCardOfCurrentPicker()
    {
        foreach (Player player in players)
        {
            if (player.currentPicker == true)
            {
                player.DisplayCard();
            }
        }
    }

    private void ShowNumberOfCardsLeft()
    {
        foreach (Player player in players)
        {
            Debug.Log("Player " + player.playerID + " has " + player.numberOfCardsLeft + " left.");
        }
    }

    private void StartNewTurn()
    {
        UpdatePlayersCardsLeft();
        ShowNumberOfCardsLeft();
        ShowAllCurrentCards();
        HideAllCards();
        ShowCardOfCurrentPicker();

        if (aiGame == true)
        {
            foreach (Player player in players)
            {
                if (player.currentPicker == true && player.playerName.Contains("CPU"))
                {
                    Player ai = player;
                    System.Random rnd = new System.Random();
                    float waitTime = rnd.Next(3, 6);
                    Invoke("AITurn", waitTime);
                }
            }
        }
    }

    private void UpdatePlayersCardsLeft()
    {
        foreach (Player player in players)
        {
            player.UpdateCardsLeft();
        }
    }

    private void AddCardsToSort()
    {
        Card[] cardsToSort = GetAllCurrentCards();

        foreach (Card card in cardsToSort)
        {
            if (!sortedUsedCards.Contains(card))
            {
                sortedUsedCards.Enqueue(card);
            }
        }
    }

    private void SortFirst()
    {
        int initCount = sortedUsedCards.Count;
        Card[] tempSortArr = new Card[sortedUsedCards.Count];
        Card temp;

        for (int i = 0; i < initCount; i++)
        {
            tempSortArr[i] = sortedUsedCards.Dequeue();
        }

        for (int x = 0; x < tempSortArr.Length; x++)
        {
            for (int y = 0; y < tempSortArr.Length - 1; y++)
            {
                if (tempSortArr[y].FirstStat > tempSortArr[y + 1].FirstStat)
                {
                    temp = tempSortArr[y + 1];
                    tempSortArr[y + 1] = tempSortArr[y];
                    tempSortArr[y] = temp;
                }
            }
        }

        for (int i = 0; i < tempSortArr.Length; i++)
        {
            sortedUsedCards.Enqueue(tempSortArr[i]);
        }
    }

    private void SortSecond()
    {
        int initCount = sortedUsedCards.Count;
        Card[] tempSortArr = new Card[sortedUsedCards.Count];
        Card temp;

        for (int i = 0; i < initCount; i++)
        {
            tempSortArr[i] = sortedUsedCards.Dequeue();
        }

        for (int x = 0; x < tempSortArr.Length; x++)
        {
            for (int y = 0; y < tempSortArr.Length - 1; y++)
            {
                if (tempSortArr[y].SecondStat > tempSortArr[y + 1].SecondStat)
                {
                    temp = tempSortArr[y + 1];
                    tempSortArr[y + 1] = tempSortArr[y];
                    tempSortArr[y] = temp;
                }
            }
        }

        for (int i = 0; i < tempSortArr.Length; i++)
        {
            sortedUsedCards.Enqueue(tempSortArr[i]);
        }
    }

    private void SortThird()
    {
        int initCount = sortedUsedCards.Count;
        Card[] tempSortArr = new Card[sortedUsedCards.Count];
        Card temp;

        for (int i = 0; i < initCount; i++)
        {
            tempSortArr[i] = sortedUsedCards.Dequeue();
        }

        for (int x = 0; x < tempSortArr.Length; x++)
        {
            for (int y = 0; y < tempSortArr.Length - 1; y++)
            {
                int year = int.Parse(tempSortArr[y].ThirdStat.Substring(6, 4));
                int month = int.Parse(tempSortArr[y].ThirdStat.Substring(3, 2));
                int day = int.Parse(tempSortArr[y].ThirdStat.Substring(0, 2));
                DateTime date = new DateTime(year, month, day);

                int wYear = int.Parse(tempSortArr[y + 1].ThirdStat.Substring(6, 4));
                int wMonth = int.Parse(tempSortArr[y + 1].ThirdStat.Substring(3, 2));
                int wDay = int.Parse(tempSortArr[y + 1].ThirdStat.Substring(0, 2));
                DateTime wDate = new DateTime(wYear, wMonth, wDay);

                if (date < wDate)
                {
                    temp = tempSortArr[y + 1];
                    tempSortArr[y + 1] = tempSortArr[y];
                    tempSortArr[y] = temp;
                }
            }
        }

        for (int i = 0; i < tempSortArr.Length; i++)
        {
            sortedUsedCards.Enqueue(tempSortArr[i]);
        }
    }

    private void SortFourth()
    {
        int initCount = sortedUsedCards.Count;
        Card[] tempSortArr = new Card[sortedUsedCards.Count];
        Card temp;

        for (int i = 0; i < initCount; i++)
        {
            tempSortArr[i] = sortedUsedCards.Dequeue();
        }

        for (int x = 0; x < tempSortArr.Length; x++)
        {
            for (int y = 0; y < tempSortArr.Length - 1; y++)
            {
                int year = int.Parse(tempSortArr[y].ThirdStat.Substring(6, 4));
                int month = int.Parse(tempSortArr[y].ThirdStat.Substring(3, 2));
                int day = int.Parse(tempSortArr[y].ThirdStat.Substring(0, 2));
                DateTime date = new DateTime(year, month, day);

                int wYear = int.Parse(tempSortArr[y + 1].ThirdStat.Substring(6, 4));
                int wMonth = int.Parse(tempSortArr[y + 1].ThirdStat.Substring(3, 2));
                int wDay = int.Parse(tempSortArr[y + 1].ThirdStat.Substring(0, 2));
                DateTime wDate = new DateTime(wYear, wMonth, wDay);

                if (date > wDate)
                {
                    temp = tempSortArr[y + 1];
                    tempSortArr[y + 1] = tempSortArr[y];
                    tempSortArr[y] = temp;
                }
            }
        }

        for (int i = 0; i < tempSortArr.Length; i++)
        {
            sortedUsedCards.Enqueue(tempSortArr[i]);
        }
    }

    private void AITurn()
    {
        Card[] cardsToTest = GetAllCurrentCards();
        Player ai = null;

        foreach (Player player in players)
        {
            if (player.currentPicker == true)
            {
                ai = player;
            }
        }

        double outOfHowMany = sortedUsedCards.Count;
        double firstDraw = 0;
        double firstLose = 0;
        double firstChanceWin = 0;
        double firstChanceDraw = 0;
        double firstChanceWinOrDraw = 0;
        double secondDraw = 0;
        double secondLose = 0;
        double secondChanceWin = 0;
        double secondChanceDraw = 0;
        double secondChanceWinOrDraw = 0;
        double thirdDraw = 0;
        double thirdLose = 0;
        double thirdChanceWin = 0;
        double thirdChanceDraw = 0;
        double thirdChanceWinOrDraw = 0;
        double fourthDraw = 0;
        double fourthLose = 0;
        double fourthChanceWin = 0;
        double fourthChanceDraw = 0;
        double fourthChanceWinOrDraw = 0;

        if (sortedUsedCards.Contains(ai.currentCard))
        {
            outOfHowMany = sortedUsedCards.Count - 1;
        }

        Debug.Log(outOfHowMany);

        SortFirst();

        foreach (Card card in sortedUsedCards)
        {
            if (ai.currentCard.FirstStat > card.FirstStat)
            {
                firstLose++;
            }
            else if (ai.currentCard.FirstStat == card.FirstStat)
            {
                if (ai.currentCard != card)
                {
                    firstDraw++;
                }
            }
        }

        Debug.Log(firstLose);
        Debug.Log(firstDraw);

        firstChanceWin = firstLose / outOfHowMany;
        firstChanceDraw = firstDraw / outOfHowMany;
        firstChanceWinOrDraw = firstChanceDraw + firstChanceWin;

        Debug.Log(firstChanceWin);
        Debug.Log(firstChanceDraw);
        Debug.Log(firstChanceWinOrDraw);

        SortSecond();

        foreach (Card card in sortedUsedCards)
        {
            if (ai.currentCard.SecondStat > card.SecondStat)
            {
                secondLose++;
            }
            else if (ai.currentCard.SecondStat == card.SecondStat)
            {
                if (ai.currentCard != card)
                {
                    secondDraw++;
                }
            }
        }

        Debug.Log(secondLose);
        Debug.Log(secondDraw);

        secondChanceWin = secondLose / outOfHowMany;
        secondChanceDraw = secondDraw / outOfHowMany;
        secondChanceWinOrDraw = secondChanceDraw + secondChanceWin;
        
        Debug.Log(secondChanceWin);
        Debug.Log(secondChanceDraw);
        Debug.Log(secondChanceWinOrDraw);

        SortThird();

        foreach (Card card in sortedUsedCards)
        {
            int year = int.Parse(ai.currentCard.ThirdStat.Substring(6, 4));
            int month = int.Parse(ai.currentCard.ThirdStat.Substring(3, 2));
            int day = int.Parse(ai.currentCard.ThirdStat.Substring(0, 2));
            DateTime date = new DateTime(year, month, day);

            int wYear = int.Parse(card.ThirdStat.Substring(6, 4));
            int wMonth = int.Parse(card.ThirdStat.Substring(3, 2));
            int wDay = int.Parse(card.ThirdStat.Substring(0, 2));
            DateTime wDate = new DateTime(wYear, wMonth, wDay);

            if (date < wDate)
            {
                thirdLose++;
            }
            else if (date == wDate)
            {
                if (ai.currentCard != card)
                {
                    thirdDraw++;
                }
            }
        }
        
        Debug.Log(thirdLose);
        Debug.Log(thirdDraw);

        thirdChanceWin = thirdLose / outOfHowMany;
        thirdChanceDraw = thirdDraw / outOfHowMany;
        thirdChanceWinOrDraw = thirdChanceDraw + thirdChanceWin;

        Debug.Log(thirdChanceWin);
        Debug.Log(thirdChanceDraw);
        Debug.Log(thirdChanceWinOrDraw);

        SortFourth();

        foreach (Card card in sortedUsedCards)
        {
            int year = int.Parse(ai.currentCard.FourthStat.Substring(6, 4));
            int month = int.Parse(ai.currentCard.FourthStat.Substring(3, 2));
            int day = int.Parse(ai.currentCard.FourthStat.Substring(0, 2));
            DateTime date = new DateTime(year, month, day);

            int wYear = int.Parse(card.FourthStat.Substring(6, 4));
            int wMonth = int.Parse(card.FourthStat.Substring(3, 2));
            int wDay = int.Parse(card.FourthStat.Substring(0, 2));
            DateTime wDate = new DateTime(wYear, wMonth, wDay);

            if (date > wDate)
            {
                fourthLose++;
            }
            else if (date == wDate)
            {
                if (ai.currentCard != card)
                {
                    fourthDraw++;
                }
            }
        }

        Debug.Log(fourthLose);
        Debug.Log(fourthDraw);

        fourthChanceWin = fourthLose / outOfHowMany;
        fourthChanceDraw = fourthDraw / outOfHowMany;
        fourthChanceWinOrDraw = fourthChanceDraw + fourthChanceWin;

        Debug.Log(fourthChanceWin);
        Debug.Log(fourthChanceDraw);
        Debug.Log(fourthChanceWinOrDraw);

        if (firstChanceWin <= .5 && secondChanceWin <= .5 && thirdChanceWin <= .5 && fourthChanceWin <= .5)
        {
            if (firstChanceWinOrDraw >= secondChanceWinOrDraw && firstChanceWinOrDraw >= thirdChanceWinOrDraw && firstChanceWinOrDraw >= fourthChanceWinOrDraw)
            {
                CompareFirstCategory(cardsToTest);
                Debug.Log("first");
            }
            else if (secondChanceWinOrDraw >= firstChanceWinOrDraw && secondChanceWinOrDraw >= thirdChanceWinOrDraw && secondChanceWinOrDraw >= fourthChanceWinOrDraw)
            {
                CompareSecondCategory(cardsToTest);
                Debug.Log("second");
            }
            else if (thirdChanceWinOrDraw >= firstChanceWinOrDraw && thirdChanceWinOrDraw >= secondChanceWinOrDraw && thirdChanceWinOrDraw >= fourthChanceWinOrDraw)
            {
                CompareThirdCategory(cardsToTest);
                Debug.Log("third");
            }
            else
            {
                CompareFourthCategory(cardsToTest);
                Debug.Log("fourth");
            }
        }
        else
        {
            if (firstChanceWin >= secondChanceWin && firstChanceWin >= thirdChanceWin && firstChanceWin >= fourthChanceWin)
            {
                CompareFirstCategory(cardsToTest);
                Debug.Log("first");
            }
            else if (secondChanceWin >= firstChanceWin && secondChanceWin >= thirdChanceWin && secondChanceWin >= fourthChanceWin)
            {
                CompareSecondCategory(cardsToTest);
                Debug.Log("second");
            }
            else if (thirdChanceWin >= firstChanceWin && thirdChanceWin >= secondChanceWin && thirdChanceWin >= fourthChanceWin)
            {
                CompareThirdCategory(cardsToTest);
                Debug.Log("third");
            }
            else
            {
                CompareFourthCategory(cardsToTest);
                Debug.Log("fourth");
            }
        }
    }

    private IEnumerator WaitForInput(bool draw)
    {
        AudioScript.PlaySound(cardTurn);
        continueText.enabled = true;
        resultText.enabled = true;

        if (draw == true)
        {
            resultText.text = "Draw!";
            cardsToAddText.enabled = true;
            cardsToAddText.text = "Cards to add: " + cardsToAdd.Count;
        }
        else
        {
            resultText.text = winningPlayer.playerName + " wins round!";
        }

        int playersWithNoCards = 0;

        while (pauseGameInput == true)
        {
            if (Input.anyKey)
            {
                pauseGameInput = false;
            }
            yield return new WaitForEndOfFrame();
        }

        foreach (Player player in players)
        {
            player.cardModel.transform.GetChild(0).GetChild(0).GetChild(2).GetComponent<UnityEngine.UI.Text>().color = Color.black;
            player.cardModel.transform.GetChild(0).GetChild(0).GetChild(2).GetComponent<UnityEngine.UI.Outline>().effectColor = Color.white;
            player.cardModel.transform.GetChild(0).GetChild(0).GetChild(3).GetComponent<UnityEngine.UI.Text>().color = Color.black;
            player.cardModel.transform.GetChild(0).GetChild(0).GetChild(3).GetComponent<UnityEngine.UI.Outline>().effectColor = Color.white;
            player.cardModel.transform.GetChild(0).GetChild(0).GetChild(4).GetComponent<UnityEngine.UI.Text>().color = Color.black;
            player.cardModel.transform.GetChild(0).GetChild(0).GetChild(4).GetComponent<UnityEngine.UI.Outline>().effectColor = Color.white;
            player.cardModel.transform.GetChild(0).GetChild(0).GetChild(5).GetComponent<UnityEngine.UI.Text>().color = Color.black;
            player.cardModel.transform.GetChild(0).GetChild(0).GetChild(5).GetComponent<UnityEngine.UI.Outline>().effectColor = Color.white;
        }

        continueText.enabled = false;
        resultText.enabled = false;

        if (aiGame == true)
        {
            AddCardsToSort();
        }

        if (draw == false)
        {
            SetCurrentCardForEachPlayer(winningPlayer);
        }
        else
        {
            SetCurrentCardForEachPlayer(null);
        }
        UpdatePlayersCardsLeft();

        foreach (Player player in players)
        {
            if (player.numberOfCardsLeft == 0)
            {
                playersWithNoCards++;
            }
        }

        if (playersWithNoCards >= numberOfPlayers - 1)
        {
            if (playersWithNoCards == numberOfPlayers)
            {
                winningPlayer = null;
            }
            GameObject.Find("MenuController").GetComponent<MenuScript>().SceneToLoad("GameOver");
        }
        else
        {
            StartNewTurn();
        }
    }

    public void StopInvoke()
    {
        CancelInvoke();
    }
}
