﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuScript : MonoBehaviour
{
    public static bool loadGame = false;
    public static int playersInSavedGame;
    public static string[] playerNames;

    public Dropdown dropDown;
    public Slider playerSlider;
    public Slider cardSlider;
    public Toggle pvaiToggle;
    public Text playerSliderText;
    public Text cardSliderText;
    public Text playerWonText;
    public Text[] playerText;

    public void Start()
    {
        if (SceneManager.GetActiveScene().name == "TitleScreen")
        {
            if (GameObject.Find("GameController") != null)
            {
                GameObject.Find("GameController").GetComponent<GameController>().StopInvoke();
            }

            GameController.aiGame = false;
            if (GameController.deck == null)
            {
                dropDown.value = 0;
                GameController.deck = "characters";
                cardSlider.maxValue = MyUtilities.LoadXMLData("characters").Length;
                cardSlider.minValue = 30;
                GameController.numberOfCardsToUse = 30;
            }
            else
            {
                if (GameController.deck == "characters")
                {
                    dropDown.value = 0;
                    cardSlider.maxValue = MyUtilities.LoadXMLData("characters").Length;
                    cardSlider.minValue = 30;
                    GameController.numberOfCardsToUse = 30;
                }
                else if (GameController.deck == "consoles")
                {
                    dropDown.value = 1;
                    cardSlider.maxValue = MyUtilities.LoadXMLData("consoles").Length;
                    cardSlider.minValue = 10;
                    GameController.numberOfCardsToUse = 10;
                }
            }

            playerNames = new string[6];
            GameController.numberOfPlayers = (int)playerSlider.value;
            cardSlider.value = cardSlider.minValue;

            if (SaveSystem.LoadCardsToAdd() == null)
            {
                GameObject.Find("LoadButton").GetComponent<Button>().interactable = false;
            }

            for (int i = 0; i < 6; i++)
            {
                if (i < playerSlider.value)
                {
                    playerText[i].color = Color.white;
                    playerText[i].transform.GetChild(0).GetComponent<InputField>().interactable = true;
                }
                else
                {
                    playerText[i].color = Color.gray;
                    playerText[i].transform.GetChild(0).GetComponent<InputField>().interactable = false;
                }
            }
        }
        else if (SceneManager.GetActiveScene().name == "GameOver")
        {
            if (GameController.winningPlayer != null)
            {
                playerWonText.text = GameController.winningPlayer.playerName + " wins!";
            }
            else
            {
                playerWonText.text = "Draw!";
            }
        }
        else if (SceneManager.GetActiveScene().name == "TopTrumps")
        {
            GameObject.Find("GameController").GetComponent<GameController>().NewGame();
        }

        if (GameObject.Find("Panel") != null)
        {
            if (GameController.deck == "characters")
            {
                GameObject.Find("Panel").GetComponent<Image>().sprite = Resources.Load<Sprite>("ssbubg");
                GameObject.Find("Panel").GetComponent<Image>().color = new Color(0.31f, 0.31f, 0.31f, 1);
            }
            else if (GameController.deck == "consoles")
            {
                GameObject.Find("Panel").GetComponent<Image>().sprite = Resources.Load<Sprite>("consolebg");
                GameObject.Find("Panel").GetComponent<Image>().color = Color.white;
            }
        }
    }

    public void ToggleChanged()
    {
        if (pvaiToggle.isOn)
        {
            playerSlider.enabled = false;
            GameController.numberOfPlayers = 2;
            playerSliderText.text = "Players: " + GameController.numberOfPlayers.ToString();
            playerSlider.value = 2;
            for (int i = 0; i < 6; i++)
            {
                if (i < playerSlider.value)
                {
                    playerText[i].color = Color.white;
                    playerText[i].text = "Player";
                    playerText[i].transform.GetChild(0).GetComponent<InputField>().interactable = true;
                    if (i == 1)
                    {
                        playerText[i].text = "CPU";
                        playerText[i].transform.GetChild(0).GetComponent<InputField>().interactable = false;
                    }
                }
                else
                {
                    playerText[i].color = Color.gray;
                    playerText[i].transform.GetChild(0).GetComponent<InputField>().interactable = false;
                }
            }
            playerSliderText.color = Color.gray;
            GameController.aiGame = true;
        }
        else
        {
            playerText[0].text = "Player 1";
            playerText[1].text = "Player 2";
            playerText[1].transform.GetChild(0).GetComponent<InputField>().interactable = true;
            playerSlider.enabled = true;
            GameController.aiGame = false;
            playerSliderText.color = Color.white;
            GameController.numberOfPlayers = (int)playerSlider.value;
        }
    }

    public void PlayerSliderValueChanged()
    {
        if (!pvaiToggle.isOn)
        {
            GameController.numberOfPlayers = (int)playerSlider.value;
            playerSliderText.text = "Players: " + GameController.numberOfPlayers.ToString();
            for (int i = 0; i < 6; i++)
            {
                if (i < playerSlider.value)
                {
                    playerText[i].color = Color.white;
                    playerText[i].transform.GetChild(0).GetComponent<InputField>().interactable = true;
                }
                else
                {
                    playerText[i].color = Color.gray;
                    playerText[i].transform.GetChild(0).GetComponent<InputField>().interactable = false;
                }
            }
        }
    }

    public void CardSliderValueChanged()
    {
        GameController.numberOfCardsToUse = (int)cardSlider.value;
        cardSliderText.text = "Cards to Use: " + GameController.numberOfCardsToUse.ToString();
    }

    public void SceneToLoad(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }

    public void LoadGame()
    {
        loadGame = true;
        GameController.numberOfPlayers = playersInSavedGame;
        SceneManager.LoadScene("TopTrumps");
    }

    public void SaveAndQuit()
    {
        Queue<Player> players = GameController.players;
        playersInSavedGame = 0;
        foreach (Player player in players)
        {
            SaveSystem.SavePlayer(player);
            playersInSavedGame++;
        }
        int i = 0;
        Card[] cardsToAdd = new Card[GameController.cardsToAdd.Count];
        foreach (Card card in GameController.cardsToAdd)
        {
            cardsToAdd[i] = card;
            i++;
        }

        SaveSystem.SaveCardsToAdd(cardsToAdd);
        SceneManager.LoadScene("TitleScreen");
    }

    public void ChangeName(int playerNumber)
    {
        playerNames[playerNumber - 1] = playerText[playerNumber - 1].GetComponentInChildren<InputField>().text;
    }

    public void ChangeDeck()
    {
        if (dropDown.value == 0)
        {
            GameObject.Find("Panel").GetComponent<Image>().sprite = Resources.Load<Sprite>("ssbubg");
            GameObject.Find("Panel").GetComponent<Image>().color = new Color(0.31f, 0.31f, 0.31f, 1);
            GameController.deck = "characters";
            cardSlider.minValue = 30;
            GameController.numberOfCardsToUse = 30;
            cardSlider.maxValue = MyUtilities.LoadXMLData("characters").Length;
            cardSlider.value = cardSlider.minValue;
        }
        else if (dropDown.value == 1)
        {
            GameObject.Find("Panel").GetComponent<Image>().sprite = Resources.Load<Sprite>("consolebg");
            GameObject.Find("Panel").GetComponent<Image>().color = Color.white;
            GameController.deck = "consoles";
            cardSlider.minValue = 10;
            GameController.numberOfCardsToUse = 10;
            cardSlider.maxValue = MyUtilities.LoadXMLData("consoles").Length;
            cardSlider.value = cardSlider.minValue;
        }
        else
        {
            System.Random rnd = new System.Random();
            int deckToUse = rnd.Next(0, 2);
            if (deckToUse == 0)
            {
                GameObject.Find("Panel").GetComponent<Image>().sprite = Resources.Load<Sprite>("ssbubg");
                GameObject.Find("Panel").GetComponent<Image>().color = new Color(0.31f, 0.31f, 0.31f, 1);
                dropDown.value = 0;
                GameController.deck = "characters";
                cardSlider.minValue = 30;
                GameController.numberOfCardsToUse = 30;
                cardSlider.maxValue = MyUtilities.LoadXMLData("characters").Length;
                cardSlider.value = cardSlider.minValue;
            }
            else
            {
                GameObject.Find("Panel").GetComponent<Image>().sprite = Resources.Load<Sprite>("consolebg");
                GameObject.Find("Panel").GetComponent<Image>().color = Color.white;
                dropDown.value = 1;
                GameController.deck = "consoles";
                cardSlider.minValue = 10;
                GameController.numberOfCardsToUse = 10;
                cardSlider.maxValue = MyUtilities.LoadXMLData("consoles").Length;
                cardSlider.value = cardSlider.minValue;
            }
        }
    }
}
