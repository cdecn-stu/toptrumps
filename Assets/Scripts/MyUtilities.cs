using UnityEngine;
using System.Xml;
using System.IO;
using System.Collections.Generic;


public class MyUtilities
{
    public static Card[] LoadXMLData(string filename)
    {
        List<Card> cards = new List<Card>();

        string name = "";
        string imageName = "";
        int firstStat = 0;
        int secondStat = 0;
        string thirdStat = "";
        string fourthStat = "";

        TextAsset xmlData = Resources.Load(filename) as TextAsset;
        string xmlString = xmlData.text;

        Card card = null;

        // Create an XmlReader
        using (XmlReader reader = XmlReader.Create(new StringReader(xmlString)))
        {
            if (filename == "characters")
            {
                while (reader.Read())
                {

                    if (reader.LocalName == "character" && reader.IsStartElement())
                    {
                        card = null;
                    }
                    if (reader.IsStartElement())
                    {
                        switch (reader.LocalName)
                        {
                            case "name":
                                name = reader.ReadElementContentAsString();
                                break;
                            case "image":
                                imageName = reader.ReadElementContentAsString();
                                break;
                            case "games":
                                firstStat = reader.ReadElementContentAsInt();
                                break;
                            case "spinoffs":
                                secondStat = reader.ReadElementContentAsInt();
                                break;
                            case "first":
                                thirdStat = reader.ReadElementContentAsString();
                                break;
                            case "latest":
                                fourthStat = reader.ReadElementContentAsString();

                                break;
                        }
                    }
                    if (reader.LocalName == "character" && !reader.IsStartElement())
                    {
                        card = new Card(name,
                                        imageName,
                                        firstStat,
                                        secondStat,
                                        thirdStat,
                                        fourthStat);
                        cards.Add(card);
                    }
                }
            }
            else if (filename == "consoles")
            {
                while (reader.Read())
                {

                    if (reader.LocalName == "console" && reader.IsStartElement())
                    {
                        card = null;
                    }
                    if (reader.IsStartElement())
                    {
                        switch (reader.LocalName)
                        {
                            case "name":
                                name = reader.ReadElementContentAsString();
                                break;
                            case "image":
                                imageName = reader.ReadElementContentAsString();
                                break;
                            case "games":
                                firstStat = reader.ReadElementContentAsInt();
                                break;
                            case "sales":
                                secondStat = reader.ReadElementContentAsInt();
                                break;
                            case "release":
                                thirdStat = reader.ReadElementContentAsString();
                                break;
                            case "latest":
                                fourthStat = reader.ReadElementContentAsString();

                                break;
                        }
                    }
                    if (reader.LocalName == "console" && !reader.IsStartElement())
                    {
                        card = new Card(name,
                                        imageName,
                                        firstStat,
                                        secondStat,
                                        thirdStat,
                                        fourthStat);
                        cards.Add(card);
                    }
                }
            }
        }

        return cards.ToArray();
    }
}
