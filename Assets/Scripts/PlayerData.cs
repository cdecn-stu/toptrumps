﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerData
{
    private Queue<Card> cards;

    public int id;
    public string name;
    public bool currentPicker;
    public int numberOfCards;

    public string[] cardsNames;
    public string[] cardsImageNames;
    public int[] cardsFirstStat;
    public int[] cardsSecondStat;
    public string[] cardsThirdStat;
    public string[] cardsFourthStat;

    public PlayerData (Player player)
    {
        cards = player.cards;
        id = player.playerID;
        name = player.playerName;
        currentPicker = player.currentPicker;
        numberOfCards = player.numberOfCardsLeft;
        cardsNames = new string[player.numberOfCardsLeft];
        cardsImageNames = new string[player.numberOfCardsLeft];
        cardsFirstStat = new int[player.numberOfCardsLeft];
        cardsSecondStat = new int[player.numberOfCardsLeft];
        cardsThirdStat = new string[player.numberOfCardsLeft];
        cardsFourthStat = new string[player.numberOfCardsLeft];

    int i = 0;
        foreach (Card card in cards)
        {
            cardsNames[i] = card.Name;
            cardsImageNames[i] = card.ImageName;
            cardsFirstStat[i] = card.FirstStat;
            cardsSecondStat[i] = card.SecondStat;
            cardsThirdStat[i] = card.ThirdStat;
            cardsFourthStat[i] = card.FourthStat;
            i++;
        }
    }
}
