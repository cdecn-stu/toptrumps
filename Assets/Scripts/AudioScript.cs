﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioScript
{
    public static AudioClip cardTurn;

    public static void PlaySound(AudioClip clip)
    {
        AudioSource audioSource = GameObject.Find("AudioController").GetComponent<AudioSource>();
        audioSource.pitch = Random.Range(0.75f, 1f);
        audioSource.PlayOneShot(clip);
    }
}
