﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Card 
{
    private string name;
    private string imageName;
    private int firstStat;
    private int secondStat;
    private string thirdStat;
    private string fourthStat;

    public string Name { get => name; }
    public string ImageName { get => imageName; }
    public int FirstStat { get => firstStat; }
    public int SecondStat { get => secondStat; }
    public string ThirdStat { get => thirdStat; }
    public string FourthStat { get => fourthStat; }

    public Card(string name, string imageName, int firstStat, int secondStat, string thirdStat, string fourthStat)
    {
        this.name = name;
        this.imageName = imageName;
        this.firstStat = firstStat;
        this.secondStat = secondStat;
        this.thirdStat = thirdStat;
        this.fourthStat = fourthStat;
    }
}
